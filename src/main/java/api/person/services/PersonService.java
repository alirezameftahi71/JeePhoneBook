package main.java.api.person.services;

import javax.validation.ValidationException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.NoContentException;
import javax.ws.rs.core.Response;

import main.java.api.person.base.BasePersonService;
import main.java.model.person.Person;
import main.java.model.person.logic.PersonManager;

public class PersonService extends BasePersonService {

	@POST
	@Path("/login")
	public Response login(Person person) {
		try {
			PersonManager personManager = new PersonManager();
			personManager.login(person);
		} catch (NoContentException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (ValidationException e) {
			return Response.status(401).entity(e.getMessage()).build();
		}
		return Response.ok().entity("Welcome dear " + person.getUsername()).build();
	}
}
