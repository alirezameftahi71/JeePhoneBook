package main.java.model.contact.logic;

import main.java.model.contact.Contact;
import main.java.model.contact.base.BaseContactManager;
import main.java.model.contact.view.ContactView;

public class ContactManager extends BaseContactManager {

	private Contact contactModel;
	private ContactView view;

	public ContactManager() {

	}

	public ContactManager(Contact contact, ContactView contactView) {
		this.contactModel = contact;
		this.view = contactView;
	}

	public void updateView() {
		view.print(contactModel);
	}

	public ContactManager(Contact contact) {
		this.contactModel = contact;
	}

	public void setStudentFirstName(String name) {
		this.contactModel.setFirstName(name);
	}
	/*
	 * TODO implement setter and getter for contact model
	 */

}
