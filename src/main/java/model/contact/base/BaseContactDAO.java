package main.java.model.contact.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.persistence.PersistenceException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import main.java.base.AbstractEntityDAO;
import main.java.model.contact.Contact;

/**
 * Log4j logger
 */
public class BaseContactDAO extends AbstractEntityDAO<Contact> {

	private static Logger logger = Logger.getLogger(BaseContactDAO.class.getName());

	protected BaseContactDAO() {

	}

	@Override
	public void add(Contact contact) throws SQLIntegrityConstraintViolationException {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(contact);
			tx.commit();
			logger.debug("contact with id = '" + contact.getId() + "' is added to Database.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			throw new SQLIntegrityConstraintViolationException(
					"Duplicate entry '" + contact.getId() + "' for key 'PRIMARY'");
		} finally {
			session.close();
		}
	}

	@Override
	public void update(Contact contact) {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.update(contact);
			tx.commit();
			logger.debug("contact with id = '" + contact.getId() + "' has updated.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Contact getById(Integer id) throws NoContentException {
		if (id == null)
			throw new NoContentException("Contact ID cannot be blank.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Contact contact = (Contact) session.get(Contact.class, id);
			tx.commit();
			if (contact == null)
				throw new NotFoundException("Entity not found for Contact ID: " + id);
			logger.debug("contact with id = '" + contact.getId() + "' was found and returned.");
			return contact;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Contact contact) {
		if (contact == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.delete(contact);
			tx.commit();
			logger.debug("contact with id = '" + contact.getId() + "' was deleted.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Collection<Contact> getAll() {
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Collection<Contact> result = session.createQuery("from Contact").list();
			tx.commit();
			if (result.size() <= 0)
				throw new NotFoundException("No Entity found in DB.");
			logger.debug("Total of " + result.size() + " contacts found in the database and were returned.");
			return result;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
