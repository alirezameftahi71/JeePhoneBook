package main.java.model.contact.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import javax.ws.rs.core.NoContentException;
import main.java.base.AbstractEntityManager;
import main.java.model.contact.Contact;

public class BaseContactManager extends AbstractEntityManager<Contact> {

	public static BaseContactManager getInstance() {
		return new BaseContactManager();
	}

	public BaseContactDAO getDAO() {
		return new BaseContactDAO();
	}

	@Override
	public void add(Contact contact) throws SQLIntegrityConstraintViolationException {
		this.getDAO().add(contact);
	}

	@Override
	public void update(Contact contact) {
		this.getDAO().update(contact);
	}

	@Override
	public void delete(Contact contact) {
		this.getDAO().delete(contact);
	}

	@Override
	public Collection<Contact> list() {
		return this.getDAO().getAll();
	}

	@Override
	public Contact getById(Integer id) throws NoContentException {
		return this.getDAO().getById(id);
	}

}
