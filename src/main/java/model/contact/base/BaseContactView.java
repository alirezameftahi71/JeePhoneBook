package main.java.model.contact.base;

import java.util.Collection;

import main.java.base.AbstractEntityView;
import main.java.model.contact.Contact;

public class BaseContactView extends AbstractEntityView<Contact> {

	@Override
	public void print(Contact contact) {
		System.out.println(contact);
	}

	@Override
	public void print(Collection<Contact> contacts) {
		for (Contact contact : contacts) {
			this.print(contact);
		}
	}
	
}
