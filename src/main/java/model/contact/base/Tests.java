package main.java.model.contact.base;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.PersistenceException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import main.java.core.MD5;
import main.java.model.Feature;
import main.java.model.Role;
import main.java.model.person.Person;

public class Tests extends BaseContactDAO {
	@Test
	public void addPerson() {
		Feature feature1 = new Feature("ADD", "/person/add/");
		Feature feature2 = new Feature("UPDATE", "/person/update/");
		Set features = new HashSet<Feature>();
		features.add(feature1);
		features.add(feature2);
		Role role = new Role("admin", features);
		role.setId(1);
		Person person = new Person();
		person.setUsername("user");
		person.setPassword(MD5.getMD5("pass"));
		person.setRole(role);

		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(person);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Test
	public void addRole() {
		Feature feature1 = new Feature("ADD", "/person/add/");
		Feature feature2 = new Feature("UPDATE", "/person/update/");
		Set features = new HashSet<Feature>();
		features.add(feature1);
		features.add(feature2);
		Role role = new Role("admin", features);

		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(role);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
