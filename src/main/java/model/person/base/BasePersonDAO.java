package main.java.model.person.base;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.NoContentException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import main.java.base.AbstractEntityDAO;
import main.java.core.MD5;
import main.java.model.Feature;
import main.java.model.Role;
import main.java.model.person.Person;

public class BasePersonDAO extends AbstractEntityDAO<Person> {

	private static Logger logger = Logger.getLogger(BasePersonDAO.class.getName());

	protected BasePersonDAO() {

	}

	@Override
	public void add(Person person) throws SQLIntegrityConstraintViolationException {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		person.setPassword(MD5.getMD5(person.getPassword()));
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(person);
			tx.commit();
			logger.debug("Person with id = '" + person.getId() + "' is added to Database.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (PersistenceException e) {
			throw new SQLIntegrityConstraintViolationException(
					"Duplicate entry '" + person.getId() + "' for key 'PRIMARY'");
		} finally {
			session.close();
		}

	}

	@Override
	public void update(Person person) {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		person.setPassword(MD5.getMD5(person.getPassword()));
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.update(person);
			tx.commit();
			logger.debug("Person with id = '" + person.getId() + "' has updated.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	@Override
	public Person getById(Integer id) throws NoContentException {
		if (id == null)
			throw new NoContentException("Person ID cannot be blank.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Person person = (Person) session.get(Person.class, id);
			tx.commit();
			if (person == null)
				throw new NotFoundException("Entity not found for Person ID: " + id);
			logger.debug("Person with id = '" + person.getId() + "' was found and returned.");
			return person;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Person person) {
		if (person == null)
			throw new IllegalArgumentException("No Content was Recieved.");
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.delete(person);
			tx.commit();
			logger.debug("Person with id = '" + person.getId() + "' was deleted.");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@Override
	public Collection<Person> getAll() {
		Transaction tx = null;
		Session session = null;
		try {
			session = getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Collection<Person> result = session.createQuery("from Person").list();
			tx.commit();
			if (result.size() <= 0)
				throw new NotFoundException("No Entity found in DB.");
			logger.debug("Total of " + result.size() + " persons found in the database and were returned.");
			return result;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

}
