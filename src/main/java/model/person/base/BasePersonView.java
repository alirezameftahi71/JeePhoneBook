package main.java.model.person.base;

import java.util.Collection;

import main.java.base.AbstractEntityView;
import main.java.model.person.Person;

public class BasePersonView extends AbstractEntityView<Person> {

	@Override
	public void print(Person person) {
		System.out.println(person);
	}

	@Override
	public void print(Collection<Person> persons) {
		for (Person person : persons) {
			this.print(person);
		}
	}
	
}
