package main.java.base;

import javax.ws.rs.core.Response;

public interface EntityService<E> {

	Response add(E e);

	Response getById(Integer id);

	Response delete(Integer id);

	Response update(E e);

	Response getAll();

}
