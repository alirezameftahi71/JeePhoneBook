package main.java.base;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public abstract class AbstractEntityDAO<E> implements EntityDAO<E> {

	public static SessionFactory getSessionFactory() {
		try {
			return new Configuration().configure("/main/resources/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

}